﻿using System;
using System.Collections.Generic;
using System.Threading;
using GTANetworkAPI;
using SimpleHttp;
using Player = GTANetworkAPI.Player;
using Vehicle = GTANetworkAPI.Vehicle;

namespace SimpleHttp
{
    public class Server
    {
        public Server()
        {
            var httpServer = new Thread(thread);
            httpServer.Start();
        }

        private void thread()
        {
            Route.Add("/scores/", (request, response, arguments) =>
            {
                String Resp = "";
                foreach (var player in NAPI.Pools.GetAllPlayers())
                {
                    Resp += $"{player.Name} - {NAPI.Data.GetEntityData(player, "Score")}\n";
                }
                response.AsText(Resp);
            });
            HttpServer.ListenAsync(5555, CancellationToken.None, Route.OnHttpRequestAsync).Wait();
        }
    }
}

namespace GTAServer
{
    public class Test : Script
    {
        Dictionary<Vehicle, List<Player>> veh_player_map = new Dictionary<Vehicle, List<Player>>();
        public Test()
        {
            Server server = new Server();
            Console.WriteLine("Server started");
        }

        [RemoteEvent("car_paint")]
        private void CarPaint(Player client, Vehicle veh)
        {
            NAPI.Data.SetEntityData(client, "Score", NAPI.Data.GetEntityData(client, "Score") + 1);
            if (veh_player_map.ContainsKey(veh))
            {
                foreach (var player in veh_player_map[veh])
                {
                    if(player == client)
                        continue;
                    NAPI.Data.SetEntityData(player, "Score", NAPI.Data.GetEntityData(client, "Score") - 1);
                }
            }
            else
                veh_player_map.Add(veh, new List<Player>());
            if(!veh_player_map[veh].Contains(client)) veh_player_map[veh].Add(client);
            var rand = new Random();
            veh.CustomPrimaryColor = veh.CustomSecondaryColor = new Color(rand.Next(0, 255), rand.Next(0, 255), rand.Next(0, 255));
        }

        [ServerEvent(Event.PlayerConnected)]
        private void OnPlayerConnect(Player client)
        {
            NAPI.Data.SetEntityData(client, "Score", 0);
        }

        [Command("veh")]
        private void createVehicle(Player player)
        {
            NAPI.Vehicle.CreateVehicle(VehicleHash.Sultan, player.Position, new Vector3(0, 0, 0), 0, 0);
        }

        [Command("gun")]
        private void Gun(Player player)
        {
            player.GiveWeapon(WeaponHash.Pistol50, 1000);
        }
    }
}